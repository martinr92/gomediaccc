// Copyright 2021 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package gomediaccc

import (
	"net/url"
	"path"
	"time"
)

const (
	conferencesURL = "https://api.media.ccc.de/public/conferences"
)

// Conferences structure with all conferences
type Conferences struct {
	Conferences []*Conference `json:"conferences"`
}

// Conference contains all data about a single conference
type Conference struct {
	Acronym             string    `json:"acronym"`
	Title               string    `json:"title"`
	EventLastReleasedAt time.Time `json:"event_last_released_at"`
	LogoURL             string    `json:"logo_url"`
	Events              []*Event  `json:"events"`
}

// LoadConferences loads all conferences from media.ccc.de
func LoadConferences() (conferences *Conferences, rawData []byte, err error) {
	// call server and parse response
	rawData, err = httpGet(conferencesURL, &conferences)
	return
}

// LoadConference loads all data for a single conference
func LoadConference(acronym string) (conference *Conference, rawData []byte, err error) {
	// build URL
	url := mustURL(url.Parse(conferencesURL))
	url.Path = path.Join(url.Path, acronym)

	// call server and parse response
	rawData, err = httpGet(url.String(), &conference)
	return
}
