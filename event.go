// Copyright 2021 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package gomediaccc

import (
	"net/url"
	"path"
	"time"
)

const (
	eventsURL = "https://api.media.ccc.de/public/events"
)

// Event contains all data for a single event
type Event struct {
	GUID            string       `json:"guid"`
	Slug            string       `json:"slug"`
	Date            time.Time    `json:"date"`
	Title           string       `json:"title"`
	Description     string       `json:"description"`
	ViewCount       int          `json:"view_count"`
	Length          int          `json:"length"`
	URL             string       `json:"url"`
	FrontendLink    string       `json:"frontend_link"`
	PosterURL       string       `json:"poster_url"`
	ConferenceTitle string       `json:"conference_title"`
	ConferenceURL   string       `json:"conference_url"`
	Persons         []string     `json:"persons"`
	Recordings      []*Recording `json:"recordings"`
}

// LoadEvent loads a single event
func LoadEvent(guid string) (event *Event, rawData []byte, err error) {
	// build URL
	url := mustURL(url.Parse(eventsURL))
	url.Path = path.Join(url.Path, guid)

	// call server and parse response
	rawData, err = httpGet(url.String(), &event)
	return
}
