// Copyright 2021 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package gomediaccc

import "strings"

// Recording contains all metadata for a single evnet recording
type Recording struct {
	Size         int    `json:"size"`
	MimeType     string `json:"mime_type"`
	Language     string `json:"language"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	FileName     string `json:"filename"`
	RecordingURL string `json:"recording_url"`
}

const (
	// RecordingMimeTypeSubtitle subtitle mime type for recording
	RecordingMimeTypeSubtitle = "application/x-subrip"
	// RecordingMimeTypeVideoMp4 mp4 mime time for recording
	RecordingMimeTypeVideoMp4 = "video/mp4"
)

// Languages returns a list of all languages for this recording
func (recording *Recording) Languages() []string {
	return strings.Split(recording.Language, "-")
}
