# gomediaccc
[![GoDoc](https://godoc.org/gitlab.com/martinr92/gomediaccc?status.svg)](https://godoc.org/gitlab.com/martinr92/gomediaccc)
[![pipeline status](https://gitlab.com/martinr92/gomediaccc/badges/master/pipeline.svg)](https://gitlab.com/martinr92/gomediaccc/commits/master)
[![coverage report](https://gitlab.com/martinr92/gomediaccc/badges/master/coverage.svg)](https://gitlab.com/martinr92/gomediaccc/commits/master)
[![codecov](https://codecov.io/gl/martinr92/gomediaccc/branch/master/graph/badge.svg)](https://codecov.io/gl/martinr92/gomediaccc)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/martinr92/gomediaccc)](https://goreportcard.com/report/gitlab.com/martinr92/gomediaccc)

Framework for metadata handling of media.ccc.de written in golang.

# License
```
Copyright 2021 Martin Riedl

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
