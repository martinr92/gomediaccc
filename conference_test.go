package gomediaccc

import "testing"

func TestLoadConferences(t *testing.T) {
	conferences, data, err := LoadConferences()
	if err != nil {
		t.Error(err)
		return
	}

	// check for returned data
	if len(data) < 1024 {
		t.Error("invalid data received")
		return
	}

	// check parsed conferences
	if len(conferences.Conferences) < 10 {
		t.Error("to less conferences parsed")
		return
	}
}

func TestLoadConference(t *testing.T) {
	conference, data, err := LoadConference("rc3")
	if err != nil {
		t.Error(err)
		return
	}

	// check for returned data
	if len(data) < 1024 {
		t.Error("invalid data received")
		return
	}

	// check parsed conference
	if conference.Acronym != "rc3" {
		t.Error("received invalid acronym")
		return
	}

	// check parsed events
	if len(conference.Events) < 10 {
		t.Error("parsed to less events")
		return
	}
}

func TestLoadInvalidConference(t *testing.T) {
	_, _, err := LoadConference("asdf1234")
	if err == nil {
		t.Error("expected error, because conference should not be found")
		return
	}
}
