// Copyright 2021 Martin Riedl
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package gomediaccc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func httpGet(url string, obj interface{}) ([]byte, error) {
	// send HTTP call
	httpResponse, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer httpResponse.Body.Close()

	// check http code
	if httpResponse.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received invalid http status code %d", httpResponse.StatusCode)
	}

	// read data from http call
	data, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return data, err
	}

	// parse JSON data
	if err := json.Unmarshal(data, &obj); err != nil {
		return data, err
	}

	return data, nil
}
