package gomediaccc

import "testing"

func TestLoadEvent(t *testing.T) {
	guid := "090c24b4-34c9-432c-a848-41150439f8c9"
	event, data, err := LoadEvent(guid)
	if err != nil {
		t.Error(err)
		return
	}

	// check for returned data
	if len(data) < 1024 {
		t.Error("invalid data received")
		return
	}

	// check parsed event
	if event.GUID != guid {
		t.Error("invalid GUID")
		return
	}

	// check title
	if event.Title != "#rC3 Eröffnung" {
		t.Error("invalid title")
		return
	}

	// check view counter
	if event.ViewCount < 1000 {
		t.Error("invalid view count")
		return
	}

	// check length
	if event.Length != 1818 {
		t.Error("invalid length")
		return
	}
}

func TestLoadInvalidEvent(t *testing.T) {
	_, _, err := LoadEvent("asdf1234")
	if err == nil {
		t.Error("expected 404 not found error")
		return
	}
}
